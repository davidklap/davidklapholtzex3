#include "Vector.h"
#include <iostream>

void Vector::printElements()
{
	for (int i = 0; i < size(); i++)
	{
		std::cout << _elements[i];
	}
	std::cout << "\n";
}
Vector::Vector(int n)
{ 
	// check that n is <= to 2 and if not chnage it to 2 
	if (n < MIN_SIZE_PRAMETER)
	{
		n = MIN_SIZE_PRAMETER;
	}
	_size = 0;
	_capacity = n;	 // add 4 mor becuse thaey f=dindt tell me how much so i use 4 
	_elements = new int[n];
	_resizeFactor = n;	
}

Vector ::~Vector()
{
	_elements = nullptr;
	delete [] this->_elements;
}

int Vector::size() const
{
	return _size;
}

int Vector::capacity() const
{
	return _capacity;
}

int Vector::resizeFactor() const
{
	return _resizeFactor;
}

bool Vector::empty()const
{
	if (size())
	{
		return false;
	}
	return true;
}

void Vector::push_back(const int& val)
{
	if (size() < capacity())
	{
		this->_elements[size()] = val;
		_size++;
		return;
	}
	int* new_elements = new int[capacity() + resizeFactor()];
	// copy the data from the ele,nts to the new elemtnts arr
	for (int i =0;i< size();i++)
	{
		new_elements[i] = _elements[i];
	}
	// add the new num
	new_elements[size()] = val;
	_size++;
	delete []	 _elements;
	_capacity += resizeFactor();
	_elements = new_elements;
}


int Vector::pop_back()
{
	// check if empty 
	if (empty())
	{
		std::cerr << " error: pop from empty vector\n";
		return EMPTY_VECTOR_RETURN;
	}
	int temp = _elements[size()];
	// whaen i dec size its like i delte the last num becusr its men the ts not in the size of the ligel nums so it like its like it delte and int the next time it wiil change it 
	_size--;
	return temp;

}

void Vector::reserve(int num)
{
	int add_capicity = num - capacity();
	if (add_capicity % resizeFactor())
	{
		
		_capacity = resizeFactor() * (add_capicity % resizeFactor() + 1);// becuse we need mor one becusr we have cerry
		return;
	}
	_capacity += resizeFactor() * (add_capicity / resizeFactor());
}

void Vector::resize(int n)
{
	if (n+size() > capacity())
	{
		reserve(n+size());
		_size = n;
		return;
	}
	_size = n;


}

void Vector::assign(int val)
{
	for (int i = 0; i < size(); i++)
	{
		_elements[i] = val;
	}
}


void Vector::resize(int n, const int& val)
{
	if (n < size())
	{
		resize(n);
		return;// becuse its mens ot not add any numbers it just delte
	}
	int first_size = size();
	resize(n);
	for (int i = 0; i < size() - first_size; i++) 
	{
		_elements[first_size +i] = val;
	}

}
Vector& Vector::operator= (const Vector& other)
{
	if (this == &other) // tries to copy the object to itself
	{
		return *this;
	}

	delete[] this->_elements; // release old memory

	// shallow copy fields
	this->_size = other._size;
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;

	// deep copy dynamic fields (pointers/arrays)
	this->_elements = new  int[other._capacity];
	for (int i = 0; i < other._size; i++)
	{
		// copies cell by cell
		this->_elements[i] = other._elements[i];
	}
	return *this;
}
 Vector::Vector(const Vector& other)
{
	 *this = other;
}

 int& Vector:: operator[](int n) const 
 {
	 if (n >= size()) // >= becuse the fact the number of index in the arr srtart from sero so the max size is size -1 becuse the zero asn i used size and not caputy becust the the aplacesees after size is unnishlize palcees and he cant gat thay valio its a garbige valio 
	 {
		 std::cerr << "unliel index try smaaler one next time firsrt index valio :	";
		 return _elements[0];
	 }
	 int& val = _elements[n];
	 return val;
 }

 /*
 i didnt knoe ehy but it namde a uge problem it stop all the progarm from working but at list i tried 
 Vector& Vector:: operator+(const Vector& other)
 {
	 Vector vec(this->capacity());
	 for (int i = 0; i < this->size();i++)
	 {
		 vec._elements[i] = this->_elements[i] + other._elements[i];
	 }
	 return vec;
 }*/